FROM eclipse-temurin:8-jdk-focal

COPY tabula.jar ./tabula.jar

CMD ["java", "-Dfile.encoding=utf-8", "-Xms256M", "-Xmx1024M", "-jar", "tabula.jar"]
